# Clase 11 – Comando mvn site (documentación)
En muchas ocasiones se nos va a solicitar la documentación de una aplicación, para esto, Maven nos permite tener tal documentación por medio del uso del comando `mvn site`. Para que funcione este comando, debemos modificar el archivo pom.xml y agregar el plugin `Apache Maven Site`.

![Colocación del plugin Apache Maven Site en el archivo pom.xml](/img/figura1_clase11.png)

Al ejecutar el comando `mvn site`, se va a generar una carpeta llamada site al interior del directorio target, ahí tendremos una sitio web con la información básica de nuestra aplicación. Una vez modificado el archivo pom.xml, cargamos los cambios de Maven y ejecutamos el comando `mvn site`. Véase las siguientes imágenes.

![Demostración de la creación de la carpeta site al interior de la carpeta target del proyecto](/img/figura2_clase11.png)

![Inicio del sitio web creado como resultado de la ejecución del comando mvn site](/img/figura3_clase11.png)

Para tener disponible la documentación de una aplicación en diferentes idiomas, podemos modificar el archivo pom.xml para configurar los idiomas deseados, el idioma por defecto es ingles y agregamos uno más, el español.

![Configuración del archivo pom.xml para tener disponible el idioma español en la documentación del proyecto](/img/figura4_clase11.png)

A partir de la configuración anterior, nuevamente cargamos los cambios de Maven y ejecutamos el comando `mvn site`, con esto, tendremos una nueva carpeta con el sitio web de la documentación en español. Esto lo podemos ver en las siguientes imágenes.

![Ejecución del comando mvn site mostrando la creación de la documentación en español e inglés](/img/figura5_clase11.png)

![Demostración de la creación de la carpeta de archivos con la documentación en español](/img/figura6_clase11.png)

![Inicio del sitio web de la documentación en idioma español](/img/figura7_clase11.png)

En algunas ocasiones también es necesario agregar el `Javadoc`, es decir, una breve documentación de los métodos, atributos y código de nuestra aplicación, para esto, debemos agregar una sección reporting en el archivo pom.xml además de ejecutar `mvn site`.

![Creación de la sección reporting en el archivo pom](/img/figura8_clase11.png)

Es importante mencionar que antes de ejecutar el comando `mvn site`, debemos establecer una variable de entorno a nivel de proyecto en IntelliJ IDEA para que funcione correctamente el comando, para esto, elegimos `File > Settings`, luego nos dirigimos a `Build, Execution, Deployment` y finalmente seleccionamos la opción `Runner` de Maven.

![Creación de una variable de entorno a nivel del proyecto](/img/figura9_clase11.png)

Para acceder al documento `Javadoc`, debemos abrir la documentación web generada y tendremos una nueva sección llamada `Project Reports`, ahí tendremos un enlace que nos va a permitir visualizar el documento en cuestión. Véase las siguientes imágenes.

![Demostración de la creación de la sección Project Reports en el sitio web de la documentación](/img/figura10_clase11.png)

![Javadoc de la clase Persona generado como resultado de agregar la sección reporting en el archivo pom](/img/figura11_clase11.png)

*Nota*: Cuando el Maven embebido de IntelliJ IDEA no nos permita usar una versión especifica de un plugin o nos diga que no la encuentra, podemos usar la opción `Run Maven Build` del botón `site` que nos ofrece el IDE para resolver esto. Veamos esto en la siguiente imagen.

![Opción Run Maven Build proporcionada por el IDE](/img/figura12_clase11.png)

**¡¡Fin de curso!!**









