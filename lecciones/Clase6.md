# Clase 6 - Comando mvn install
En muchas ocasiones vamos a tener lógica de negocio o algunas clases que deseamos reutilizar en otros proyectos, para realizar esto podemos usar el comando `mvn install`. 
El comando `mvn install` instala un paquete (package) de un proyecto en el repositorio local, para usarlo como una dependencia en otros proyectos localmente. 

## Ejemplo
En el caso de Debian de Linux, el paquete obtenido como resultado de usar `mvn install` se coloca en la ruta `/home/nombreUsuario/.m2/repository`. La siguiente imagen muestra los archivos generados como resultado de usar el comando `mvn install` para un proyecto de Maven llamado `SimpleIntelliJMaven`.

![Paquete generado como resultado de ejecutar el comando mvn install en un proyecto de Maven](/img/figura1_clase6.png)

Ahora, en otro proyecto (en el que deseemos usar las clases de `SimpleIntelliJMaven`) que llamamos `SimpleIntelliJMavenConsume`, en el archivo pom.xml, vamos a colocar como dependencia los datos del proyecto requerido:

![Colocación de un proyecto local de Maven como dependencia de otro en el archivo pom.xml](/img/figura2_clase6.png)

Una vez colocada la dependencia, vamos a cargar los cambios de Maven y el IDE nos va a mostrar la referencia del proyecto que necesitamos.

![IntelliJ IDEA nos muestra que está usando como depedendencia un proyecto local y la coloca en sus librerias externas requeridas](/img/figura3_clase6.png)

Con lo anterior, ya tenemos la disponibilidad de usar las clases de `SimpleIntelliJMaven` al interior de `SimpleIntelliJMavenConsume`.

## Usar diferentes versiones de código de un proyecto a reutilizar
Es importante saber que podemos usar diferentes versiones de código del proyecto que vamos a consumir (`SimpleIntelliJMaven`), para esto, modificamos el código en la parte que deseemos y después establecemos una versión para la aplicación por medio de la etiqueta `<version>` del archivo pom.xml, una vez hecho esto, debemos usar el comando `mvn clean` y luego `mvn install` para poder generar el paquete de la versión que hemos creado. En la siguiente imagen se muestra que hemos creado tres versiones de código y por tanto se crean 3 paquetes de nuestro proyecto.

![Diferentes versiones de paquetes de un proyecto, generados como parte de haber creado 3 versiones de código del mismo](/img/figura4_clase6.png)

Como dato adicional, podemos usar los comandos  `mvn clean` y `mvn install` desde IntelliJ IDEA, en la sección Maven del IDE:

![Uso de los comandos clean e install desde IntelliJ IDEA](/img/figura5_clase6.png)

Para consumir o usar el código de `SimpleIntelliJMaven` en sus diferentes versiones debemos definir la versión que queremos utilizar en la sección dependencias del archivo pom.xml de la aplicación que utilizamos para consumir, esto se muestra en  la siguiente imagen.

![Colocación de las diferentes versiones de un proyecto local usado como dependencia de otro proyecto](/img/figura6_clase6.png)

Una vez establecida la versión que queremos consumir, cargamos los cambios de Maven y consumimos el código en cuestión, por ejemplo, en la versión 3 de SimpleIntelliJMaven se tiene un atributo llamado edad que no está en la versiones anteriores y podemos usarlo para establecer una edad de la persona además de imprimir este dato por pantalla haciendo uso de un método toString, esto se puede observar en la siguiente imagen.

![Ejemplo de consumo de código de un proyecto local al interior de otro proyecto](/img/figura7_clase6.png)
