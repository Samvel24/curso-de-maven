# Clase 8 - Múltiples POM

En algunas ocasiones es posible que tengamos mas de un archivo pom.xml, además, probablemente tengamos un configuración diferente en cada archivo, esto nos permite indicarle a Maven el archivo particular que deseamos utilizar.
Para poder usar un archivo pom.xml diferente del original, en la linea de comandos, primero vamos a ejecutar el comando `mvn clean` para limpiar los archivos y directorios generados por Maven durante una compilación previa y después podemos usar el comando `mvn -f nombrePom.xml package`, con esto podemos ejecutar el proyecto pero usando un archivo pom.xml especifico.
En la siguiente imagen tenemos un archivo llamado pom_2.xml al que se le ha establecido una versión 2 de proyecto, por tanto, al ejecutar el comando previamente descrito, por pantalla se va a mostrar que la versión de proyecto es la número 2.

![Ejemplo de archivo pom diferente al original y uso del comando que le indica a Maven que debe usar tal archivo](/img/figura1_clase8.png)

