# Clase 2 - Instalación de Maven

En esta clase se instaló Maven, para esto, se utilizó la terminal de Linux y el comando `sudo apt-get install maven`:

![Instalación de Maven en la terminal](/img/figura1_clase2.png)