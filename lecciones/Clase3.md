# Clase 3 -  Crear un nuevo proyecto

Para crear un nuevo proyecto con Maven, debemos hacer uso del siguiente comando:  

`mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false`

Analicemos las diferentes partes de este comando:  

- mvn archetype:generate, este es el comando en si mismo, nos permite generar esqueletos de proyectos Maven de diferentes tipos, como .jar, aplicaciones web, etc.  

- Opción -DgroupId, nos ayuda a identificar de forma única un proyecto dentro de todos los proyectos.  

- Opción -DartifactId, es el nombre del archivo jar o war que hemos compilado y que no tiene versión.  
    
- Opción -DarchetypeArtifactId, nos ayuda a definir el tipo de aplicación de Java, si no colocamos esta opción, va a tomar un valor por defecto que genera una aplicación Java simple, sin embargo, podemos establecer otro valor para, por ejemplo, crear una aplicación web.
Para tener en cuenta otro tipo de aplicaciones que se pueden crear con Maven ver [Introduction to Archetypes](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html)  

- Opción -DinteractiveMode, al colocar esta opción en false, maven ya no nos va a preguntar las opciones que deseamos configurar sino que va a asumir varias por defecto.  

El comando `mvn compile` genera una carpeta llamada target que contiene los archivos.class que se obtienen como resultado de la compilación del proyecto.  

El comando `mvn package` empaqueta todo el proyecto (previamente compilado) y genera un archivo ejecutable, un jar, por ejemplo.  

Al posicionarnos en la carpeta target de un proyecto que hemos empaquetado, podemos ejecutar la aplicación por medio del siguiente comando:  

`java -cp nombreArchivoJar.jar ubicacionMetodoMain`

Por ejemplo:  
![Ejemplo de ejecución de una aplicación por medio de la linea de comandos](/img/figura1_clase3.png)  

Para limpiar la carpeta target de un proyecto podemos usar el comando `mvn clean`, con esto ya no disponemos de esta carpeta ni de su contenido.  

Todo lo anterior nos permite ver que Maven nos permite crear proyectos de forma estandarizada y práctica para trabajar con Java.  

Para saber más sobre los diferentes comandos de Maven ver: [Introduction to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html).