# Clase 4 - POM.xml

El archivo **pom.xml** es el núcleo de la configuración de un proyecto en Maven. Es un único archivo de configuración que contiene la mayoría de la información necesaria para construir un proyecto de la forma que se desea.  

El POM es enorme y puede ser abrumador en su complejidad, pero no es necesario comprender todas las complejidades para usarlo de manera efectiva.
POM significa *Modelo de objetos de proyecto* (*Project Object Model*). Es una representación XML de un proyecto Maven contenido en un archivo llamado **pom.xml**.  

Una de las partes más importantes del archivo pom es la sección `dependencies`, en esta sección se pueden colocar las librerías que están en la página Maven Repository. La siguiente imagen muestra un ejemplo de un archivo **pom.xml** con la sección `dependencies` remarcada en color azul.  

![Ejemplo de archivo pom.xml](/img/figura1_clase4.png)  

Es importante mencionar que algunas dependencias contienen la etiqueta `<scope>`, esto se refiere al ámbito de una dependencia, así como también a cómo limitar la transitividad de estas. Por ejemplo,el ámbito runtime indica que una librería no es necesaria para el momento de la compilación pero si lo es para la ejecución, a saber, una librería JDBC es un ejemplo de dependencia perteneciente a este ámbito.  

En conclusión de esta clase, el archivo **pom.xml** va a tener definidas todas las librerías o dependencias que necesita nuestro proyecto para que sea más portable, por ejemplo, cuando deseamos empaquetar (*mvn package*) un proyecto desde diferentes equipos o computadoras.  

Para saber más sobre el archivo **pom.xml**, ver:
- [POM Reference](https://maven.apache.org/pom.html)
- [Maven in 5 Minutes - The POM](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html#the-pom)