# Clase 5 - Usar Maven en los IDEs NetBeans, Spring Tool Suite o Intellij IDEA

Es importante resaltar que cada IDE tienen un Maven embebido, además, este tipos de IDEs tiene opciones para poder apuntar a un Maven que hemos instalado en una computadora y de esta manera usar uno que no es el que viene por defecto.

## Configuración de Maven
Por ejemplo, en Netbeans, podemos acceder a las diferentes opciones para Maven a través de la pestaña `Tools > Options` y luego nos dirigimos a `Java > Maven`.

![Configuración de Maven en Netbeans](/img/figura1_clase5.png)

En Spring Tool Suite podemos acceder a las diferentes opciones para Maven a través de la pestaña `Window > Preferences` y luego nos dirigimos a `Maven > User Settings`.

![Configuración de Maven en Spring Tool Suite](/img/figura9_clase5.png)

En IntelliJ IDEA podemos acceder a las diferentes opciones para Maven a través de la pestaña `File > Settings` y luego nos dirigimos a `Build, Execution, Deployment`.

![Configuración de Maven en IntelliJ IDEA](/img/figura2_clase5.png)

## Crear un nuevo proyecto de Maven
Para crear un nuevo proyecto de Maven por medio de Netbeans seleccionamos `File > New Project` y después elegimos `Java with Maven` como se muestra en la siguiente imagen.

![Crear nuevo proyecto de Maven con Netbeans](/img/figura3_clase5.png)

Después de lo anterior, elegimos un nombre para la aplicación, con esto, se va a crear un proyecto con su respectiva estructura y el archivo pom.xml.

![Nuevo proyecto de Maven generado con Netbeans](/img/figura4_clase5.png)

Analizando un poco más las etiquetas del archivo pom.xml, específicamente `<maven.compiler.source>` y `<maven.compiler.target>`, `target` indica la versión de la JVM sobre la que vamos a ejecutar y `source` proporciona compatibilidad de sintaxis con una versión especificada de Java. Por ejemplo, podemos usar la sintaxis de la JDK 7 (`source`) y correrla sobre un motor de JVM cuya versión de Java es 8 (`target`), sin embargo, no podríamos usar código con lambdas de Java 8 (`source`) y ejecutarlo sobre un motor de Java 7 (`target`).

Para crear un nuevo proyecto de Maven por medio de Spring Tool Suite seleccionamos `New  > Maven Project`, después elegimos la ubicación del proyecto y también seleccionamos el Arquetipo para el proyecto.

![Crear nuevo proyecto de Maven con Spring Tool Suite](/img/figura10_clase5.png)

Finalmente, elegimos el Group Id, Artifact Id y la versión de nuestro proyecto.

![Parametros para un proyecto de Maven con Spring Tool Suite](/img/figura11_clase5.png)

Para crear un nuevo proyecto de Maven por medio de IntelliJ IDEA seleccionamos `File > New > Project` y después elegimos la opción `New Project` en la ventana en cuestión.

![Crear nuevo proyecto de Maven con IntelliJ IDEA](/img/figura5_clase5.png)

Con esto, tendremos creado un proyecto de Maven listo para ser desarrollado.

![Nuevo proyecto de Maven generado con IntelliJ IDEA](/img/figura6_clase5.png)

## Importar un proyecto existente de Maven

Para importar un proyecto ya existente de Maven por medio de Netbeans seleccionamos `File > Open Project` y después elegimos la carpeta del proyecto deseado.

![Importar proyecto de Maven ya existente con Netbeans](/img/figura7_clase5.png)

Para importar un proyecto ya existente de Maven por medio de Spring Tool Suite seleccionamos `File > Import` y después en la ventana que sale elegimos `Maven > Existing Maven Projects`.

![Importar proyecto de Maven ya existente con Netbeans](/img/figura12_clase5.png)

Por último, en Spring Tool Suite, debemos elegir la carpeta que contiene el proyecto deseado, o bien, abrimos la carpeta que contiene el proyecto y seleccionar el pom.xml para que el IDE pueda importar la aplicación.

Para importar un proyecto ya existente de Maven por medio de IntelliJ IDEA seleccionamos `File > Open` y después elegimos la carpeta del proyecto deseado.

![Importar proyecto de Maven ya existente con IntelliJ IDEA](/img/figura8_clase5.png)


