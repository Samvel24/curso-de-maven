# Clase 10 – Excluir dependencias
En algunas ocasiones debido a un conflicto o requerimiento, es posible que necesitemos excluir alguna dependencia.  

Para demostrar la exclusión de una dependencia, por ejemplo, de `Opentest4j` (descargada como dependencia transitiva en la lección anterior), en IntelliJ IDEA, podemos usar la combinación `ctrl+clic` en el groupId o el artifactId de la dependencia `JUnit` y podremos acceder al pom.xml de esta, ahí podemos tomar el  groupId y el artifactId  de `Opentest4j` y establecer su exclusión en el archivo pom.xml del proyecto `SimpleTestMaven` de la clase anterior. Esto se muestra en la siguiente imagen.

![Exclusión de la dependencia transitiva Opentest4j en el archivo pom.xml](/img/figura1_clase10.png)

Con lo anterior, cargamos los cambios de Maven y entonces se va a excluir la dependencia `Opentest4j` en la carpeta de dependencias del proyecto.

![Demostración de la exclusión de la dependencia transitiva Opentest4j en las liberias externas del proyecto](/img/figura2_clase10.png)