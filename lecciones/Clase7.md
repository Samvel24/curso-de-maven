# Clase 7 - Perfiles (Profiles)

Los perfiles son una característica importante en Maven, se usan mucho en ambientes laborales, estos pueden ayudar en la personalización de las compilaciones para un entorno particular, por ejemplo, producción o desarrollo.

## Archivos de propiedades estándar de Java

Para esta lección es importante conocer los archivos de propiedades, son archivos de texto con la extensión .properties, estos utilizan la sintaxis Key=Value (Clave=Valor). Las claves son nombres para cosas que aparecen en el código fuente, con valores que designan aquellas cosas que se mostrarán al usuario.

## Ejemplo

En esta lección se han creado tres archivos de propiedades, los podemos categorizar para el ambiente de producción, desarrollo y calidad (qa). Estos archivos se pueden ver en la siguiente imagen:

![Archivos de propiedades creados para ser usados en diferentes entornos](/img/figura1_clase7.png)

En el archivo pom.xml podemos relacionar sus configuraciones con los diferentes archivos de propiedades, para esto, se deben agregar diferentes secciones, en una de ellas es donde indicamos que el ambiente por defecto es el del archivo environment.properties (dedicado para entorno de desarrollo en este caso).

![Colocación de configuración en archivo pom.xml para establecer el ambiente por defecto](/img/figura2_clase7.png)

Así mismo, hemos definido una sección de configuración de empaquetado del proyecto, en ella, principalmente, indicamos que al momento de la compilación, solo se va a considerar un archivo de propiedades dedicado a un entorno particular.

![Colocación de configuración en archivo pom.xml para hacer uso de un solo archivo de propiedades al momento de la compilación](/img/figura3_clase7.png)

Además, hemos definido dos secciones, una para un perfil de calidad y otra para uno de producción, en ambas se usa un plugin de Maven para mostrar por pantalla el tipo de perfil que estará activo.

![Colocación de perfil de calidad (qa) en archivo pom.xml](/img/figura4_clase7.png)

![Colocación de perfil de producción (prod) en archivo pom.xml](/img/figura5_clase7.png)

Para realizar una ejecución y prueba de los diferentes perfiles que hemos establecido podemos usar el comando `mvn -P nombrePerfil package`. Véase la ejecución de los diferentes perfiles en las siguientes imágenes.

![Uso del comando para ejecutar y probar el perfil de calidad](/img/figura6_clase7.png)

![Uso del comando para ejecutar y probar el perfil de producción](/img/figura7_clase7.png)

Para saber más sobre la teoría de esta lección, visitar los siguientes enlaces:
- [Maven profiles](https://www.jetbrains.com/help/idea/work-with-maven-profiles.html)
- [Properties files](https://www.jetbrains.com/help/idea/properties-files.html)
- [NetBeans System Properties Module Tutorial - Introducing the Sources](https://netbeans.apache.org/tutorials/60/nbm-nodesapi.html#_introducing_the_sources)