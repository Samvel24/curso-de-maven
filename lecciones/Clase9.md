# Clase 9 - Dependencias transitivas
Es importante conocer que cuando usamos dependencias en el pom.xml, estas pueden necesitar de otras subdependencias, también conocidas como **dependencias transitivas**.
En esta lección vamos a trabajar con un nuevo proyecto llamado `SimpleTestMaven`, en él, al interior de su archivo pom.xml, vamos a colocar el requerimiento de la dependencia `JUnit Jupiter API`.

Una vez que hayamos cargado los cambios de Maven para descargar `JUnit Jupiter API`, podremos observar que no solamente se encuentra tal dependencia sino otras que no definimos explícitamente en el pom.xml, esto sucede porque JUnit necesita de otras dependencias para funcionar. Esto se muestra en la siguiente imagen, en rojo la dependencia principal y en verde las dependencias transitivas.

![JUnit y sus subdependencias en IntelliJ IDEA](/img/figura1_clase9.png)

Para observar de forma más visual las diferentes dependencias y su jerarquía, podemos usar el botón de Maven que nos ofrece IntelliJ IDEA, específicamente en la opción Dependencies. Véase la siguiente imagen.

![Jerarquía de dependencias en IntelliJ IDEA](/img/figura2_clase9.png)