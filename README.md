# Curso de Maven
Notas de clase del curso de Maven de MitoCode  

[Clic aquí para acceder a la lista de reproducción en YouTube ](https://www.youtube.com/playlist?list=PLvimn1Ins-40atMWQkxD8r8pRyPLAU0iQ)

- [Clase 1 - Introducción](lecciones/Clase1.md)
- [Clase 2 - Instalación de Maven](lecciones/Clase2.md)
- [Clase 3 - Crear un nuevo proyecto](lecciones/Clase3.md)
- [Clase 4 - POM.xml](lecciones/Clase4.md)
- [Clase 5 - Usar Maven en los IDEs NetBeans o Intellij IDEA](lecciones/Clase5.md)
- [Clase 6 - Comando mvn install](lecciones/Clase6.md)
- [Clase 7 - Perfiles (Profiles)](lecciones/Clase7.md)
- [Clase 8 - Múltiples POM](lecciones/Clase8.md)
- [Clase 9 - Dependencias transitivas](lecciones/Clase9.md)
- [Clase 10 - Excluir dependencias](lecciones/Clase10.md)
- [Clase 11 - Comando mvn site (documentación)](lecciones/Clase11.md)